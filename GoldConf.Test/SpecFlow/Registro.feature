﻿Feature: Registro
	Se debe registrar en el sistema GoldConf. 
	Llenar datos necesario y cumpliendo algunas restricciones. 
	Finalmente, dar en el botón registrar.

@mytag
Scenario: Registro exitoso
	Given He elegido registrarme

	And Debería ver un mensaje de saludo personalizado.

	When Me registro con datos válidos

	Then Debería recibir un correo electrónico de confirmación.

@mytag
Scenario: Registro no exitoso
	Given He elegido registrarme

	And Debería ver un mensaje de saludo personalizado.

	When Ingrese la contraseña 123

	Then Me pide una contraseña mayor o igual a 6 caracteres.
