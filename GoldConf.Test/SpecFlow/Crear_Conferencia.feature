﻿Feature: Crear_Conferencia
	El usuario debe de estar logueado como administrador.
	El usuario administrador puede crear una conferencia.
@mytag
Scenario: Conferencia registrado correctamente.
	Given El administrador decidio registrar un ponente
	And Deberia llenar todos los datos solicitados
	When Ingresé los datos válidos
	Then Me mostró un mensaje de registro correcto y se creo la coferencia
