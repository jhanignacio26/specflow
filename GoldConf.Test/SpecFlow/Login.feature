﻿Feature: Login
	Llenar datos necesario, como usuario y contraseña.
	Finalmente dar en el botón Ingresar. 

@mytag
Scenario: Login correcto
	Given He elegido loguearme

	And Debería pedirme mi usuario y contraseña

	When Ingreso mi usuario y contraseña, luego doy click en el botón "Ingresar"

	Then Debería redireccionarme a la página principal

@mytag
Scenario: Login incorrecto
	Given He elegido loguearme

	And Debería pedirme mi usuario y contraseña

	When Ingrese mi usuario correctamente, pero mi contraseña no y di click en el botón "Ingresar"

	Then No me redireccionarme a la página principal y me volvió a pedir contraseña y contraseña