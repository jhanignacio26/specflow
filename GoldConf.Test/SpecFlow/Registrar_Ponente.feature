﻿Feature: Registrar_Ponente
	El usuario debe de estar logueado como administrador.
	El usuario administrador puede registrar a un ponente.

@mytag
Scenario: Ponente registrado correctamente.
	Given El administrador decidio registrar un ponente
	And Debería llenar todos los datos
	When Ingresé los datos válidos
	Then Me mostró un mensaje de registro correcto y se agregó el ponente

@mytag
Scenario: Ponente no registrado correctamente.
	Given El administrador decidio registrar un ponente
	And Deberia llenar todos los datos
	When No ingrese el correo electrónico del ponente
	Then No se logro registrar el ponente y me pidio ingresar el correo

